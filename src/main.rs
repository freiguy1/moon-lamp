#![deny(unsafe_code)]
#![no_std]
#![no_main]

mod ashi;

use cortex_m_rt::entry;
// use cortex_m_semihosting::hprintln;
use ds323x::{Ds323x, Rtcc};
use embedded_hal::digital::v2::InputPin;
use moon_lamp::{MoonLamp, TICK_FREQUENCY_HZ, TIME_UPDATE_INTERVAL_SEC};
use nb::block;
use panic_halt as _;
use stm32f1xx_hal::{
    gpio,
    i2c::{BlockingI2c, Mode},
    pac,
    pac::SPI2,
    prelude::*,
    spi::{NoMiso, NoSck, Spi, Spi2NoRemap},
    timer::Timer,
};
use ws2812_spi::Ws2812;

type Leds = Ws2812<
    Spi<
        SPI2,
        Spi2NoRemap,
        (
            NoSck,
            NoMiso,
            gpio::gpiob::PB15<gpio::Alternate<gpio::PushPull>>,
        ),
        u8,
    >,
>;

#[entry]
fn main() -> ! {
    // Get access to the core peripherals from the cortex-m crate
    let cp = cortex_m::Peripherals::take().unwrap();
    // Get access to the device specific peripherals from the peripheral access crate
    let dp = pac::Peripherals::take().unwrap();

    // Take ownership over the raw flash and rcc devices and convert them into the corresponding
    // HAL structs
    let mut flash = dp.FLASH.constrain();
    let mut rcc = dp.RCC.constrain();

    // Freeze the configuration of all the clocks in the system and store the frozen frequencies in
    // `clocks`
    let clocks = rcc
        .cfgr
        .sysclk(24.mhz())
        .pclk1(16.mhz())
        .freeze(&mut flash.acr);

    // Set up I2C for external RTC
    let mut gpiob = dp.GPIOB.split(&mut rcc.apb2);
    let mut afio = dp.AFIO.constrain(&mut rcc.apb2);
    let scl = gpiob.pb6.into_alternate_open_drain(&mut gpiob.crl);
    let sda = gpiob.pb7.into_alternate_open_drain(&mut gpiob.crl);
    let i2c = BlockingI2c::i2c1(
        dp.I2C1,
        (scl, sda),
        &mut afio.mapr,
        Mode::Standard {
            frequency: 100.khz().into(),
        },
        clocks.clone(),
        &mut rcc.apb1,
        1_000, // start_timeout_us
        10,    // start_timeout
        1_000, // addr_timeout_us
        1_000, // data_timeout_us
    );
    let mut rtc = Ds323x::new_ds3231(i2c);

    // Use this code to set the external RTC when needed
    // let current_utc = NaiveDate::from_ymd(2020, 12, 29).and_hms(19, 45, 00);
    // rtc.set_datetime(&current_utc).unwrap();

    // cortex_m::interrupt::free(|cs| *G_RTC.borrow(cs).borrow_mut() = Some(rtc));

    // Set up night-only switch input
    let night_only_pin = gpiob.pb0.into_pull_down_input(&mut gpiob.crl);

    // Set up SPI for LEDs
    let pins = (
        NoSck,
        NoMiso,
        gpiob.pb15.into_alternate_push_pull(&mut gpiob.crh),
    );

    let spi = Spi::spi2(
        dp.SPI2,
        pins,
        ws2812_spi::MODE,
        3.mhz(),
        clocks,
        &mut rcc.apb1,
    );

    let mut leds = Ws2812::new(spi);

    // Set up the EXTI to allow for internal RTC alarm (section 18.4.2)
    let exti = dp.EXTI;
    exti.rtsr.write(|w| w.tr17().set_bit());
    exti.imr.write(|w| w.mr17().set_bit());

    // Set up timer for main loop
    let mut timer = Timer::syst(cp.SYST, &clocks).start_count_down(TICK_FREQUENCY_HZ.hz());

    #[allow(unsafe_code)]
    unsafe {
        cortex_m::peripheral::NVIC::unmask(pac::Interrupt::RTCALARM);
    };

    let mut time_refresh_counter = TICK_FREQUENCY_HZ * TIME_UPDATE_INTERVAL_SEC;
    let mut ashi = ashi::BluePill { leds: &mut leds };
    let mut moon_lamp = MoonLamp::new(&mut ashi);

    loop {
        if time_refresh_counter == TICK_FREQUENCY_HZ * TIME_UPDATE_INTERVAL_SEC {
            time_refresh_counter = 0;
            let current_date_time = match rtc.get_datetime() {
                Ok(cdt) => cdt,
                _ => {
                    continue
                }
            };
            moon_lamp.update_current_time(current_date_time);
            // hprintln!("refresh").unwrap();
        }
        moon_lamp.tick(night_only_pin.is_low().unwrap_or(true));
        time_refresh_counter += 1;

        block!(timer.wait()).unwrap();
    }
}
