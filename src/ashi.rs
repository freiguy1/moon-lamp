// use cortex_m_semihosting::hprintln;
use moon_lamp::{Ashi, NUM_LEDS};
use smart_leds::{SmartLedsWrite, RGB8};

pub struct BluePill<'a> {
    pub leds: &'a mut crate::Leds,
}

impl<'a> Ashi for BluePill<'a> {
    fn set_leds(&mut self, data: [RGB8; NUM_LEDS]) {
        self.leds.write(data.iter().cloned()).unwrap();
    }
}
