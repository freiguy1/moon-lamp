#![no_std]

use chrono::{Datelike, Duration, Timelike};
// use cortex_m_semihosting::hprintln;
use ds323x::{NaiveDate, NaiveDateTime};
use smart_leds::RGB8;
use wyhash::wyrng;

pub const NUM_LEDS: usize = 18;
pub const TIME_UPDATE_INTERVAL_SEC: u32 = 10;
pub const TICK_FREQUENCY_HZ: u32 = 30;

const TURN_LEDS_OFF_TIME: u32 = 7 * 60 + 15; // <hour> * 60 + <minute>
const TURN_LEDS_ON_TIME: u32 = 19 * 60 + 45; // <hour> * 60 + <minute>
const SECONDS_PER_CYCLE: i64 = 2_551_392;
const TWINKLE_MAX_TICKS: u8 = 120;
const TWINKLE_MIN_TICKS: u8 = 40;
const TWINKLE_HALF_WAY_TICKS: u8 = (TWINKLE_MAX_TICKS - TWINKLE_MIN_TICKS) / 2 + TWINKLE_MIN_TICKS;
const TWINKLE_NUM_LIMIT: usize = 5;
const TWINKLE_MAX_BURSTS: u8 = 4;
const ANIMATION_TICKS: u8 = 40;

const MOON_COLOR: RGB8 = RGB8 {
    r: 0x10,
    g: 0x10,
    b: 0x03,
};

const EMPTY_COLOR: RGB8 = RGB8 {
    r: 0x00,
    g: 0x00,
    b: 0x08,
};

const TWINKLE_COLOR: RGB8 = RGB8 {
    r: 0x50,
    g: 0x50,
    b: 0x50,
};

// Application-specific hardware interface
pub trait Ashi {
    fn set_leds(&mut self, leds: [RGB8; NUM_LEDS]);
}

#[derive(Copy, Clone)]
struct Twinkler {
    pub ticks_left: u8,
    pub total_ticks: u8,
    pub index: u8,
    pub bursts: u8,
}

impl Twinkler {
    fn brightness(&self) -> f32 {
        let burst_length = self.total_ticks / self.bursts;
        let shifted_ticks_left = self.ticks_left % burst_length;
        Self::mirrored_parabola(shifted_ticks_left, burst_length)
    }

    fn mirrored_parabola(x: u8, duration: u8) -> f32 {
        let half_ticks = (duration / 2) as f32;
        let x_f32 = x as f32;
        let difference_from_half = if half_ticks > x_f32 {
            half_ticks - x_f32
        } else {
            x_f32 - half_ticks
        };
        let scaled_ticks = (half_ticks - difference_from_half) / 50.;
        scaled_ticks * scaled_ticks
    }
}

pub struct MoonLamp<'a> {
    ashi: &'a mut dyn Ashi,
    twinklers: [Option<Twinkler>; TWINKLE_NUM_LIMIT],
    phase: Phase,
    is_night_only_mode: bool,
    is_night: bool,
    random_seed: u64,
    animation_countdown: u8,
}

impl<'a> MoonLamp<'a> {
    pub fn new<T: Ashi>(ashi: &'a mut T) -> Self {
        Self {
            ashi,
            twinklers: [None; TWINKLE_NUM_LIMIT],
            phase: Phase::New,
            is_night: false,
            is_night_only_mode: true,
            random_seed: 3,
            animation_countdown: ANIMATION_TICKS,
        }
    }

    pub fn tick(&mut self, is_night_only_mode: bool) {
        let is_turning_on = !self.is_night && self.is_night_only_mode && !is_night_only_mode;
        if is_turning_on {
            self.animation_countdown = ANIMATION_TICKS;
        }

        self.is_night_only_mode = is_night_only_mode;

        if self.animation_countdown > 0 {
            self.run_animation();
        } else {
            self.update_twinklers();
            self.update_leds();
        }
    }

    pub fn update_current_time(&mut self, now_utc: NaiveDateTime) {
        // Receives the time from the hardware at regular interval
        // Calculate Central Standard Time (with daylight savings implemented)
        let winter_cst = now_utc - Duration::hours(6);
        let summer_cst = now_utc - Duration::hours(5);
        let cst = if winter_cst < find_dst_begin(winter_cst.year() as u16).and_hms(2, 0, 0) {
            winter_cst
        } else if summer_cst < find_dst_end(summer_cst.year() as u16).and_hms(2, 0, 0) {
            summer_cst
        } else {
            winter_cst
        };

        // turn off from 7am to 7pm
        let minute_of_day = cst.hour() * 60 + cst.minute();
        let is_night = minute_of_day < TURN_LEDS_OFF_TIME || minute_of_day > TURN_LEDS_ON_TIME;

        let is_turning_on = !self.is_night && self.is_night_only_mode && is_night;
        if is_turning_on {
            self.animation_countdown = ANIMATION_TICKS;
        }
        self.is_night = is_night;

        // Do calculations to find moon phase
        // Map moon phase to array of color information
        let new_moon_utc = NaiveDate::from_ymd(2020, 11, 15).and_hms(5, 07, 0); // Relatively recent new moon (empty sky)
        let diff = now_utc - new_moon_utc;
        let seconds_into_current_cycle = diff.num_seconds() % SECONDS_PER_CYCLE;
        self.phase = Phase::from_seconds_into_cycle(seconds_into_current_cycle);
    }

    fn update_twinklers(&mut self) {
        // Progress twinklers
        for mut t in self.twinklers.iter_mut().filter_map(|t| t.as_mut()) {
            t.ticks_left -= 1;
        }

        // Remove dead ones
        for t in self.twinklers.iter_mut().filter(|t| t.is_some()) {
            if t.unwrap().ticks_left == 0 {
                // hprintln!("removing twinkler").unwrap();
                *t = None;
            }
        }

        if self.twinklers.iter().all(|t| t.is_some()) {
            self.update_leds();
            return;
        }

        let full_random = wyrng(&mut self.random_seed);
        let rand_1 = full_random as u16;
        let rand_2 = (full_random >> (1 * 16)) as u16;
        let rand_3 = (full_random >> (2 * 16)) as u16;
        let rand_4 = (full_random >> (3 * 16)) as u16;

        let create_new_twinkler = rand_1 % 18 == 0;
        if create_new_twinkler {
            // Can unwrap, we know that at least one is none due to shortcircuit above
            let index = self.twinklers.iter().position(|t| t.is_none()).unwrap();
            let mut led_index = (rand_2 % NUM_LEDS as u16) as u8;
            while self
                .twinklers
                .iter()
                .any(|t| t.map(|t2| t2.index == led_index).unwrap_or(false))
            {
                led_index += 3;
                if led_index >= NUM_LEDS as u8 {
                    led_index = led_index % NUM_LEDS as u8;
                }
            }

            let total_ticks = ((rand_3 % (TWINKLE_MAX_TICKS - TWINKLE_MIN_TICKS) as u16)
                + TWINKLE_MIN_TICKS as u16) as u8;

            // Use lower max bursts if total_ticks are lower
            let twinkle_max_bursts = if total_ticks < TWINKLE_HALF_WAY_TICKS {
                TWINKLE_MAX_BURSTS / 2
            } else {
                TWINKLE_MAX_BURSTS
            };

            let bursts = ((rand_4 % twinkle_max_bursts as u16) + 1) as u8;

            // hprintln!("new twink {}, {}, {}", led_index, total_ticks, random_num).unwrap();

            self.twinklers[index] = Some(Twinkler {
                ticks_left: total_ticks,
                total_ticks: total_ticks,
                index: led_index,
                bursts: bursts,
            });
        }
    }

    fn run_animation(&mut self) {
        // quickly goes through all moon phases.
        let fraction_into_cycle =
            (ANIMATION_TICKS - self.animation_countdown) as f32 / ANIMATION_TICKS as f32;
        let phase: Phase = fraction_into_cycle.into();
        let led_flags = phase.leds();
        let mut leds = [RGB8::default(); NUM_LEDS];
        for (i, &l) in led_flags.iter().enumerate() {
            if l {
                leds[i] = MOON_COLOR;
            } else {
                leds[i] = EMPTY_COLOR;
            }
        }
        self.ashi.set_leds(leds);
        self.animation_countdown -= 1;
    }

    fn update_leds(&mut self) {
        // If in night only mode and it's day, then turn off.
        if self.is_night_only_mode && !self.is_night {
            self.ashi.set_leds([RGB8::default(); NUM_LEDS]);
            return;
        }

        let led_flags = self.phase.leds();
        let mut leds = [RGB8::default(); NUM_LEDS];
        for (i, &l) in led_flags.iter().enumerate() {
            let twinkler = self
                .twinklers
                .iter()
                .filter_map(|&t| t)
                .find(|t| t.index == i as u8);
            if l {
                leds[i] = MOON_COLOR;
            } else if let Some(t) = twinkler {
                let brightness = t.brightness();
                leds[i] = percent_between_colors(brightness, EMPTY_COLOR, TWINKLE_COLOR);
            } else {
                leds[i] = EMPTY_COLOR;
            }
        }
        self.ashi.set_leds(leds);
    }
}

fn percent_between_colors(percent: f32, color_1: RGB8, color_2: RGB8) -> RGB8 {
    RGB8 {
        r: percent_between_u8s(percent, color_1.r, color_2.r),
        g: percent_between_u8s(percent, color_1.g, color_2.g),
        b: percent_between_u8s(percent, color_1.b, color_2.b),
    }
}

fn percent_between_u8s(percent: f32, num_1: u8, num_2: u8) -> u8 {
    let difference = num_2 as i8 - num_1 as i8;
    let add_amount = percent * difference as f32;
    num_1 + (add_amount + 0.5) as u8
}

fn find_dst_begin(year: u16) -> NaiveDate {
    let march_1 = NaiveDate::from_ymd(year as i32, 3, 1);
    let known_sunday = NaiveDate::from_ymd(2020, 12, 20);
    let difference_days = (march_1 - known_sunday).num_days();
    let march_1_day_of_week = difference_days % 7;
    let second_sunday = if march_1_day_of_week == 0 {
        8
    } else {
        15 - march_1_day_of_week
    };
    NaiveDate::from_ymd(year as i32, 3, second_sunday as u32)
}

fn find_dst_end(year: u16) -> NaiveDate {
    let nov_1 = NaiveDate::from_ymd(year as i32, 11, 1);
    let known_sunday = NaiveDate::from_ymd(2020, 12, 20);
    let difference_days = (nov_1 - known_sunday).num_days();
    let nov_1_day_of_week = difference_days % 7;
    let second_sunday = if nov_1_day_of_week == 0 {
        1
    } else {
        8 - nov_1_day_of_week
    };
    NaiveDate::from_ymd(year as i32, 11, second_sunday as u32)
}

enum Phase {
    New = 0,
    WaxingCrescent = 1,
    FirstQuarter = 2,
    WaxingGibbous = 3,
    Full = 4,
    WaningGibbous = 5,
    ThirdQuarter = 6,
    WaningCrescent = 7,
}

impl Phase {
    fn from_seconds_into_cycle(seconds_into_cycle: i64) -> Self {
        // this is ugly but it is interacting with real life, so I guess that happens. Check note
        // in readme.
        if seconds_into_cycle < 165615 {
            Self::New
        } else if seconds_into_cycle < 496845 {
            Self::WaxingCrescent
        } else if seconds_into_cycle < 883685 {
            Self::FirstQuarter
        } else if seconds_into_cycle < 1176135 {
            Self::WaxingGibbous
        } else if seconds_into_cycle < 1497060 {
            Self::Full
        } else if seconds_into_cycle < 1796460 {
            Self::WaningGibbous
        } else if seconds_into_cycle < 2097468 {
            Self::ThirdQuarter
        } else if seconds_into_cycle < 2400084 {
            Self::WaningCrescent
        } else {
            Self::New
        }
    }

    fn leds(&self) -> [bool; NUM_LEDS] {
        let mut result = [false; NUM_LEDS];
        match self {
            Self::New => (),
            Self::WaxingCrescent => {
                for &i in [13, 14, 15, 16, 17].iter() {
                    result[i] = true
                }
            }
            Self::FirstQuarter => {
                for &i in [9, 10, 11, 12, 13, 14, 15, 16, 17].iter() {
                    result[i] = true
                }
            }
            Self::WaxingGibbous => {
                result = [true; NUM_LEDS];
                for &i in [0, 1, 2, 3, 4].iter() {
                    result[i] = false
                }
            }
            Self::Full => {
                result = [true; NUM_LEDS];
            }
            Self::WaningGibbous => {
                result = [true; NUM_LEDS];
                for &i in [13, 14, 15, 16, 17].iter() {
                    result[i] = false
                }
            }
            Self::ThirdQuarter => {
                result = [true; NUM_LEDS];
                for &i in [9, 10, 11, 12, 13, 14, 15, 16, 17].iter() {
                    result[i] = false
                }
            }
            Self::WaningCrescent => {
                for &i in [0, 1, 2, 3, 4].iter() {
                    result[i] = true
                }
            }
        }

        result
    }
}

impl From<f32> for Phase {
    fn from(p: f32) -> Self {
        // Consider micromath crate for nice f32 math things
        let i = (p * 8. + 0.5) as u8 % 8;
        match i {
            0 => Self::New,
            1 => Self::WaxingCrescent,
            2 => Self::FirstQuarter,
            3 => Self::WaxingGibbous,
            4 => Self::Full,
            5 => Self::WaningGibbous,
            6 => Self::ThirdQuarter,
            7 => Self::WaningCrescent,
            _ => unreachable!(),
        }
    }
}
