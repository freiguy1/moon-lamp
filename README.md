# Moon Lamp

I've had this idea to make a night light for my childrens' rooms which shows the current lunar phase. The plan right now is to build it with several RGB LEDs arranged in a circle. Then with the help of a real time clock, a microcontroller running my code can calculate the current lunar phase and enable only the LEDs to make the correct moon shape.

![animated](assets/animated.gif "Animated image")

## Features

1. LEDs indicate the current moon phase with the capability to show 8 phases.
2. In "empty" sections of the moon, the LEDs are dark blue. Also randomized twinkling lights appear in the empty areas.
3. The real time clock module has an auxiliary battery so it will keep the current time when unplugged for probably decades. Plug this lamp in after being stored for a few years, and the moon should be correct
4. UTC is stored on the real time clock module and there exists logic to convert to my timezone: CST. This is helpful for programming behavior at certain times of the day
5. Using the local time from the previous step, the LEDS turn off during the day. This behavior can be disabled with a toggle switch.

## Other Feature Ideas

1. Ability to set date and time.
2. A "sleep training" feature which will indicate to children when it's time to sleep or wake.

## Code

I am using [Rust](https://rust-lang.org) for this project. I really like rust and I think it has a lot of potential for embedded development. Right now the metaphorical edges are a bit rough, but I like the challenge and appreciate using a 21st century language that compiles straight to the metal. Another great advantage rust has over C/C++ is a repository of libraries which will make communication with peripherals easy. For example [smart-leds](https://crates.io/crates/smart-leds) and [ds323x](https://crates.io/crates/ds323x).

## Materials List

The main components are a microcontroller, RTC module, and LEDs. Here is what I've chosen:

- Microcontroller: [STM32F103 Blue Pill](https://www.ebay.com/itm/264321827486) $4
    - I chose the blue pill because I've used it [before](https://gitlab.com/freiguy1/timer), it's quite powerful (which may actually be required due to the LEDs needing a pretty fast serial frequency), and very cheap.
    - Other options are something less powerful like an 8-bit AVR chip or something more powerful like a Raspberry Pi. Even though I like the AVR chips for their simplicity and low cost, I've never used rust on them and am not sure how well it'd work. While a Raspberry Pi would be great for many reasons (especially the ability to develop remotely through wifi), it's very much overkill in processing power, RAM, and cost for this project.
- LEDs: [WS2812B 5V Addressable LED Strip](https://www.ebay.com/itm/264781589797) $10-20
    - Apparently this version of LEDs can be bought with varying length between the LEDs. Order according to how spaced out you'd like your LEDs to be.
- RTC: [DS3231](https://www.ebay.com/itm/382781644430) $5

Other materials are required which are common for most embedded projects:
- A breadboard for prototyping
- Some wire and solder
- A 5V power source
- A toggle switch for dis/enabling night-only mode

## Calculating Lunar Phase

Here is some rust code that I tinkered with in [play.rust-lang.org](https://play.rust-lang.org) which calculates the current phase. Keep in mind that this code uses the [chrono](https://crates.io/crates/chrono) library which may not be useful on an embedded device.

```rust
const SECONDS_PER_CYCLE: i64 = 2_551_443;
let new_moon = Utc.ymd(2020, 11, 15).and_hms(5, 07, 0); // Relatively recent new moon (empty sky)
let now = Utc::now();
let diff = now - new_moon;
let seconds_into_current_phase = diff.num_seconds() % SECONDS_PER_CYCLE;
let fraction_into_current_phase = seconds_into_current_phase as f32 / SECONDS_PER_CYCLE as f32;
```

The code results in a `fraction_into_current_phase` float which is betwen 0.0 and 1.0 which indicates how far through the cycle the moon currently is. For instance a value of 0.5 would be a full moon since it's halfway through the cycle.

**EDIT:** After using the moon lamp for a few weeks, it seemed like my algorithm was off for calculating the current moon phase. After some digging, I learned that the full moon doesn't happen exactly half way through the cycle. It's more like 53%. With my linear algorithm, the moon lamp showed the "full moon" phase early. The actual full moon would fall on the tail end of the moon lamp's phase, if that makes sense. I constructed the table below from one particular cycle in January of 2021. The items in parenthesis I've calculated.

| Phase           | Seconds through cycle | Seconds range (calculated) |
|-----------------|-----------------------|----------------------------|
| New             |                      0|                   < 165615 |
| Waxing Crescent |               (331230)|                   < 496845 |
| First Quarter   |                 662460|                   < 883685 |
| Waxing Gibbous  |              (1004910)|                  < 1176135 |
| Full            |                1347360|                  < 1497060 |
| Waning Gibbous  |              (1646760)|                  < 1796460 |
| Third quarter   |                1946160|                  < 2097468 |
| Waning Crescent |              (2248776)|                  < 2400084 |

## Physical LED Arrangement

At the beginning, I thought a grid-style circle (imagine building a circle of in Minecraft) would be best. I've since scrapped the grid-style idea for a different approach which is a bit more organic. It has the advantages of using less LEDs, being simpler, and looking more moon-like but the disadvantage of being coupled tightly to a moon shape so it won't have the capability of showing other shapes like scrolling text.

![diagram](assets/diagram.png "LED Arrangement Diagram")

This arrangement uses 18 LEDs, which is close to half what the 7x7 circle required.

# Developer Setup

In this section I'll go over what I had to do to get this computer development-ready. It's a pretty new installation of Arch Linux, so I haven't done rust embedded development yet. I do however have rust installed which I think I did with `pacman -S rustup` and then used the rustup tool to install stable.

Next, install debugging/emulator/flashing tools: `sudo pacman -S arm-none-eabi-gdb qemu-arch-extra openocd`

Add the cross compilation target for the blue pill board: `rustup target install thumbv7m-none-eabi`

Created a file at `/etc/udev/rules.d/70-st-link.rules` with contents
```
# STM32F3DISCOVERY rev A/B - ST-LINK/V2
ATTRS{idVendor}=="0483", ATTRS{idProduct}=="3748", TAG+="uaccess"

# STM32F3DISCOVERY rev C+ - ST-LINK/V2-1
ATTRS{idVendor}=="0483", ATTRS{idProduct}=="374b", TAG+="uaccess"
```

and reloaded udev rules with `sudo udevadm control --reload-rules`.

Had to tell gdb that it is safe to load .gdbinit from current dir: `echo "set auto-load safe-path $(pwd)" >> ~/.gdbinit`

# Run

Plug in device, open terminal and run `openocd -f interface/stlink-v2.cfg -f target/stm32f1x.cfg`. Hopefully this connects smoothly, if not, attempt to hit restart button or hold it for a sec on blue pill. If it doesn't connect smoothly, time to do some internet searching? Next run `cargo run` with or without `--release` flag. This should upload the program and initiate debugging session.

## Binary size

Prerequisites: 

```
$ cargo install cargo-binutils
$ rustup component add llvm-tools-preview
```

To get just the size of the binary use the command `cargo size --release`. The BluePill has 64K. As of right now, the program is using about one quarter of that. Some binary size optimizations weren't possible with the sensitive timing the LEDs require.


